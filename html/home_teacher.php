<?php
//外部オブジェクト
require "class/database.php";

$data = new connect;
//イベントのデータを取得
$ev_data = $data->event();
?>
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ホーム/教員用</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="datepicker/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="css/card.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="datepicker/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="datepicker/locales/bootstrap-datepicker.ja.min.js"></script>
	<script src="js/calendar.js"></script>
	<!-- =======カレンダーをページ読み込み時に実行======= -->
	<script>
		$(function(){
			setCalendar();
		});
	</script>
	<!-- ============================================ -->
	<script>
		//phpのデータを受け取る
		var ev_data = '<?php echo $ev_data; ?>';
		var ev_data = JSON.parse(ev_data);
	</script>
</head>

<body>
	<div class="container">
		<div class="page-header">
			<h1 class="text-center">学生管理システム/教員</h1>
		</div>
	</div>
</body>

</html>

<?php

//セッション開始
session_start();
error_reporting( E_ERROR | E_WARNING | E_PARSE );

//最終アクセス時間からセッション管理
if ( isset( $_SESSION[ 'LAST_ACTIVITY' ] ) && ( time() - $_SESSION[ 'LAST_ACTIVITY' ] > 3900 ) ) {
	// 最終リクエスト時刻から30分経過した
	session_unset(); //
	session_destroy(); // セッション破棄
}
$_SESSION[ 'LAST_ACTIVITY' ] = time(); // 最終リクエスト時刻を更新


//セッションからデータ受け取り
$id = $_SESSION[ "id" ];
$pass = $_SESSION[ "pass" ];

$status = array('出席','遅刻','欠席','就活','病欠','公欠','早退');
$yasumi = array('登校日','休日');
// $save = $_SESSION['save'];
//
// //cookieに保存
// if($save == 'on'){
//     setcookie('my_id', $id, time() + 60 * 60 * 24 * 14);
//     setcookie('my_pass', $pass, time() + 60 * 60 * 24 * 14);
//
// }else{
//     setcookie('my_id','');
//     setcookie('my_pass','');
// }


//データベース接続
$link = mysqli_connect( "localhost", "daisuke", "miyagawa", "PBLD" );
if ( !$link ) {
	print( mysqli_connect_error( $link ) );
	exit();
}

if ( isset( $_POST[ "logout" ] ) ) {
	session_destroy();
	setcookie( 'save', 'off', time() + 60 * 60 * 24 * 14, '/' );
	header( 'Location: ./login.php' );
	exit();
}

//SELECT文
$result = mysqli_query( $link, "SELECT * FROM user WHERE id = '$id' AND pass ='$pass'" );
if ( !$result ) {
	print( mysqli_error( $link ) );
	exit();
}

$row = mysqli_fetch_assoc( $result );

//ログイン失敗の処理
if ( $row[ id ] == null ) {
	echo '
      <div class="container">
        <h3>ログインできませんでした。</h3>
          <div>
            <form action="" method="POST">
              <button type="submit" value="buck" name="button" class="btn btn-default btn-lg">戻る</button>
            </form>
          </div>
        </div>
      ';
	$but = htmlspecialchars( $_POST[ "button" ], ENT_QUOTES, "UTF-8" );
	switch ( $but ) {
		case "buck":
			session_destroy();
			setcookie( 'save', 'off', time() + 60 * 60 * 24 * 14, '/' );
			header( "Location: login.php" );
			break;
	}
	//================  ログイン失敗処理ここまで ================

	//ログイン許可時の処理
} else {

	//初期パスワードの場合
	if ( $id == $pass ) {
		header( 'Location: http://localhost:8080/change_pass.php' );
		exit;
	}

	echo '
  <div class="container" style="margin-top: -40px;">
    <div class="page-header">
      <div align="right">
        <h3>
          <form action="" method="post">
            ' . $row[ name ] . '  <!-入力ID(名前)を表示 ->
            <button type="submit" name="logout" class="btn btn-info" id="logout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</button>
          </form>
        </h3>
      </div>
    </div>

  <div class=" col-sm-2">
    <ul class="nav nav-pills nav-stacked">
      <li role="presentation" class="active"><a href="#menu1" data-toggle="tab"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
      <li role="presentation"><a href="#menu2" data-toggle="tab"><span class="glyphicon glyphicon-calendar"></span> 登校日変更</a></li>
			<li role="presentation"><a href="#menu3" data-toggle="tab"><span class="glyphicon glyphicon-edit"></span> 出欠状況管理</a></li>
      <li role="presentation"><a href="#menu4" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> 新年度生徒登録</a></li>
      <li role="presentation"><a href="#menu5" data-toggle="tab"><span class="glyphicon glyphicon-wrench"></span> パスワード変更</a></li>
    </ul>
  </div>
';

//どの出欠状況修正するか
if(isset($_POST["gakusei"])){

  $gaku_id = $_POST["gaku_id"];
  $mydate = $_POST["mydate"];

}

//出欠状況修正
if(isset($_POST["syusei"])){

  $gaku_id = $_POST["gaku_id"];
  $mydate = $_POST["mydate"];

   $per1 = $_POST["per1"];
   $per2 = $_POST["per2"];
   $per3 = $_POST["per3"];
   $per4 = $_POST["per4"];
   $per5 = $_POST["per5"];
   $reason = $_POST["reason"];

  //attの更新
  $result = mysqli_query( $link, "UPDATE att SET per1 = '$per1', per2 = '$per2', per3 = '$per3', per4 = '$per4', per5 = '$per5', reason = '$reason' WHERE id = '$gaku_id' AND day = '$mydate'" );
  if ( !$result ) {
    print( mysqli_error( $link ) );
  }

}

//単体で登校日変更
if(isset($_POST["toukoubi1"])){

	$mydate = $_POST["mydate"];

	mysqli_free_result( $result );

	$result = mysqli_query( $link, "UPDATE cal SET yasumi = ABS(yasumi-1) WHERE day = '$mydate'" );
	if ( !$result ) {
		print( mysqli_error( $link ) );
	}
}
//連続で登校日変更
if(isset($_POST["toukoubi2"])){

	$daystart = $_POST["start"];
	$dayend = $_POST["end"];

	mysqli_free_result( $result );

	$result = mysqli_query( $link, "UPDATE cal SET yasumi = ABS(yasumi-1) WHERE day >= '$daystart' AND day <= '$dayend' AND youbi <> '土' AND youbi <> '日'" );
	if ( !$result ) {
		print( mysqli_error( $link ) );
	}
}

$day=date('Y-m-d');
$year=date('Y');
$month=date('m');
$day2=date('d');


//変数dayを生成
if(isset($_POST["tod"])){
	$year=$_POST["year"];
	$month=$_POST["month"];
	$day2=$_POST["day"];

		$day=$year."-".$month."-".$day2;
		$_SESSION["day"]=$year."-".$month."-".$day2;


}


//前日の出席
if(isset($_POST["yest"])){
	// echo $day;
	if(isset($_SESSION["day"])){
		$yes_ts = strtotime($_SESSION["day"]);
	}else{
		$yes_ts = strtotime($day);
	}
	$year=date("Y",strtotime("-1 day",$yes_ts));
	$month=date("m",strtotime("-1 day",$yes_ts));
	$day2=date("d",strtotime("-1 day",$yes_ts));
		$day=$year."-".$month."-".$day2;
		$_SESSION["day"]=$year."-".$month."-".$day2;

}

//明日の出席
if(isset($_POST["tom"])){
	// echo $day;
	if(isset($_SESSION["day"])){
		$yes_ts = strtotime($_SESSION["day"]);
	}else{
		$yes_ts = strtotime($day);
	}
	$year=date("Y",strtotime("1 day",$yes_ts));
	$month=date("m",strtotime("1 day",$yes_ts));
	$day2=date("d",strtotime("1 day",$yes_ts));
		$day=$year."-".$month."-".$day2;
		$_SESSION["day"]=$year."-".$month."-".$day2;
}

	//SELECT文
	$result = mysqli_query( $link, "SELECT * FROM cal WHERE day = '$day'" );
	if ( !$result ) {
		print( mysqli_error( $link ) );
		exit();
	}
	$row = mysqli_fetch_assoc( $result );

	echo '
<!--選択タブ内のコンテンツ-->
<div class="col-sm-10 col-md-10 tab-content">
<!- ================= ホーム(教員)/学生表示 ================= ->
    <div class="tab-pane active" id="menu1">
		<form action="" method="POST" class="form-inline">
				<select name="year" class="form-control-my">';
				optionLoop("1950", date("Y"),date("Y"));
				 echo '
				 </select>
				 年
				<select name="month" class="form-control-my">';
				optionLoop('1', '12', date("m"));
				echo '</select>
				 月
				<select name="day" class="form-control-my">';
				optionLoop("1", "31", date("d"));
				echo '</select>
				日
				<button  type="submit"  name="tod" class="btn btn-success">日時決定</button>
			</form>

        <!--出席状況の簡易確認-->
				<form action="" method="POST">
				<h3>
					学生一覧
					<button type="submit"  name="yest" id="yest" class="btn btn-default">前日</button>
					<button type="submit"  name="tom" id="tom" class="btn btn-default">明日</button>
					</h3>
			</form>
				<h4>'.$year.'年'.$month.'月'.$day2.'日の出欠状況：'.$yasumi[$row[yasumi]].'</h4>
          <div class="row">
';

	// 結果セットを開放します
	mysqli_free_result( $result );

	//SELECT文
	//$result = mysqli_query( $link, " select user.id,user.name,att.per1,att.per2,att.per3,att.per4,att.per5,att.day,att.reason from user left outer join att on user.id = att.id where user.id regexp'^(0K)'  group by name order by id asc;" );
	//SELECT文
	//userとattを結合、生徒だけを、userにあるid、名前でグループ、idの昇順、
	$result = mysqli_query( $link, " select user.id,user.name,att.per1,att.per2,att.per3,att.per4,att.per5,att.day,att.reason from user left outer join att on user.id = att.id and att.day = '$day' where user.id regexp'^(0K)'  order by id asc;
" );
	if ( !$result ) {
		print( mysqli_error( $link ) );
		exit();
	}

	while ( $row = mysqli_fetch_assoc( $result ) ) {
		echo '
  <!- カード管理 ->
  <div class="col-xs-12 col-sm-6" style="margin-bottom: 20px;" >
    <article class="card " ontouchstart="">
      <div class="card-block">
        <h3 class="card-title"> ' . $row[ id ] . '</h3>
        <h4 class="text-muted">' . $row[ name ] . '</h4>
        <table class="table table-striped table-responsive">
          <tbody>
            <tr>
              <td>1限</td>
              <td>2限</td>
              <td>3限</td>
              <td>4限</td>
              <td>5限</td>
							<td>欠席理由</td>
            </tr>
						<tr>
				 ';

				 if($row[per1] == null){
				   echo '<center><b>出席データが存在しません<br></b></center>';
				 }
				 for($i = 0; $i < 5; $i++){
					 echo '<td>'.$status[$row[per.($i + 1)]].'</td>';
		       if($i == 4){
						 echo '<td>'. $status[$row[reason]];
						 if ($row[reason] == 6) {
						 	echo '(' .$row[backtime]. ')';
						 }

						 '</td>';
					 }
		     };

   echo '
	 </tr>
          </tbody>
        </table>
      </div>
    </article>
  </div>

    ';
		echo "";
	}

	echo '
      </div>
    </div>
<!- ================= 学生表示ここまで =================->

<!- ================= 登校日管理 =================->
    <div class="tab-pane" id="menu2">

		<form action="" method="POST">

		<div class="form-group">
    	<label for="mydate">日付１：</label>
    	<input type="text" class="form-control" name="mydate" readonly>
		</div>
		<button  type="submit"  name="toukoubi1">日時決定</button>

		<div class="input-daterange input-group">
    	<input type="text" class="input-sm form-control" name="start" readonly>
    	<span class="input-group-addon">～</span>
    	<input type="text" class="input-sm form-control" name="end" readonly>
		</div>
		<button  type="submit"  name="toukoubi2">日時決定</button>

		</form>

    </div>
<!- ================= 登校日管理ここまで =================->

<!- ================= 出欠状況管理 =================->
    <div class="tab-pane" id="menu3">

		<form action="" method="POST">

		<div class="form-group">
		  <label for="mydate">日付１：</label>
		  <input type="text" class="form-control" name="mydate" readonly>
		</div>
		<!--データベース学生番号-->
		学生番号：
		<select name="gaku_id">';

		//学生IDを取得
		$result = mysqli_query( $link, "SELECT * FROM user where id regexp'^(0K)'" );
		if ( !$result ) {
			print( mysqli_error( $link ) );
			exit();
		}

		while($row = mysqli_fetch_assoc( $result )){
		echo '<option value="'.$row[id].'">'.$row[id];
		}

		echo'
		</select>
		<br><button type="submit" name="gakusei">決定</button>
		</form>
		';

		//SELECT文
		$result = mysqli_query( $link, "SELECT * FROM att WHERE id = '$gaku_id' AND day = '$mydate'" );
		if(!$result ) {
			print( mysqli_error( $link ) );
			exit();
		}

		$row = mysqli_fetch_assoc( $result );

		if($row[per1] == null){
			echo '<center><b>出席データが存在しません<br></b></center>';
		}else{

		echo '
		<form action="" method="POST">
		<table class="table table-striped table-responsive">
		   <tbody>
		     <tr>
		       <td>1限</td>
		       <td>2限</td>
		       <td>3限</td>
		       <td>4限</td>
		       <td>5限</td>
		       <td>欠席理由</td>
		     </tr>
		     <tr>
		     ';

		//1限から５限のSELECTを作成
		for($i=1;$i<6;$i++){

		  echo '<td><select name="per'.$i.'">';

		  //出席、遅刻、欠席のOPTIONを作成
		  for($j=0;$j<3;$j++){

		    if($row[per.$i] == $j){
		    echo '<option value="'.$j.'" selected>'.$status[$j];
		    }else{
		    echo '<option value="'.$j.'">'.$status[$j];
		    }

		  }

		  echo '</select></td>';
		}

		echo '<td><select name="reason">';

		echo '<option value="">--';

		//欠席、就活、病欠、公欠のOPTIONを作成
		for($i=2;$i<=6;$i++){
		  if($row[reason] == $i){
		    echo '<option value="'.$i.'" selected>'.$status[$i];
		  }else{
		    echo '<option value="'.$i.'">'.$status[$i];
		  }
		}

		echo '
		</td>
		</select>
		</tr>
		</tbody>
		</table>

		<input type="hidden" name="gaku_id" value="'.$row[id].'">
		<input type="hidden" name="mydate" value="'.$row[day].'">

		<button  type="submit"  name="syusei">修正</button>
		</from>';

	}
	//ifend

echo '
	 	<h3>出欠状況一覧</h3>
			<div id="result"></div>
    </div>
<!-  ================= 出欠状況管理ここまで =================->

<!- ================= 新年度登録 =================->
    <div class="tab-pane" id="menu4">
	 	<h3>新年度生徒登録</h3>
		<form action="upload.php" method="post" enctype="multipart/form-data">
		  CSVファイル：<br />
		  <input type="file" name="csvfile" size="30">
		  <button type="submit">アップロード</button>
		</form>
    </div>
<!-  ================= 新年度登録ここまで =================->

<!- ================= パスワード変更 =================->
    <div class="tab-pane" id="menu5">
      <div class="container">
          <a href="change_pass.php">パス変更</a>
      </div>
    </div>
<!- ================= パスワード変更ここまで =================->
</div>
';
}
?>

<script>
$(".form-control").datepicker({
	language: "ja",
	format: "yyyy-mm-dd"
});
</script>
<script>
$(".input-daterange").datepicker({
	language: "ja",
	format: "yyyy-mm-dd"
});
</script>

<?php
//セレクトオプションのループ設定
function optionLoop($start, $end, $value = null){

	for($i = $start; $i <= $end; $i++){
		if(isset($value) &&  $value == $i){
			echo "<option value=\"{$i}\" selected=\"selected\">{$i}</option>";
		}else{
			echo "<option value=\"{$i}\">{$i}</option>";
		}
	}
}

?>
