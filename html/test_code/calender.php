<?php
//外部オブジェクト
require "class/database.php";

//データベースを扱うオブジェクト
$data = new connect;
//attのデータを取得
//
$test = $data->test("SELECT * FROM att");

?>
﻿<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Event Calendar Widget</title>

      <link rel="stylesheet" href="css/calender.css">

</head>

<body>
  <div id="calendar"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>

    <script type="text/javascript">

    !function() {

  //  document.write(<?php echo $test; ?>);

      //外部の使ってるかもしれない（moment.js）
      var today = moment();

      //メイン処理
      //表示する場所（idで指定）、予定のデータが引数
      function Calendar(selector, events) {
        this.el = document.querySelector(selector);
        this.events = events;
        //currentは今日
        this.current = moment().date(1);
        //this.holiday = moment().date(1);
        this.draw();
        //今日作成
        var current = document.querySelector('.today');
        if(current) {
          var self = this;
          window.setTimeout(function() {
            self.openDay(current);
          }, 500);
        }
        //休日作成.holiday
        /*
        var holiday = document.querySelector('.holiday');
        if(holiday) {
          var self = this;
          window.setTimeout(function() {
            self.openDay(holiday);
          }, 500);
        }
        */

      }

      //ヘッダーと月を取り敢えず生成
      Calendar.prototype.draw = function() {
        //Create Header
        this.drawHeader();

        //Draw Month
        this.drawMonth();
      }

    　//ヘッダーの具体的な作成
      Calendar.prototype.drawHeader = function() {
        var self = this;
        if(!this.header) {
          //Create the header elements
          this.header = createElement('div', 'header');
          this.header.className = 'header';

          this.title = createElement('h1');

          //押して月の切り替え
          var right = createElement('div', 'right');
          right.addEventListener('click', function() { self.nextMonth(); });

          var left = createElement('div', 'left');
          left.addEventListener('click', function() { self.prevMonth(); });

          //appendChildは更新
          //Append the Elements
          this.header.appendChild(this.title);
          this.header.appendChild(right);
          this.header.appendChild(left);
          this.el.appendChild(this.header);
        }
        //月、年のフォーマットを表示してる？
        this.title.innerHTML = this.current.format('MMMM YYYY');
      }

      //月
      Calendar.prototype.drawMonth = function() {
        if(!this.month) {
          this.month = createElement('div', 'month');
          this.el.appendChild(this.month);
        }

        this.month.innerHTML = '';
        this.backFill();
        this.currentMonth();
        this.fowardFill();
      }

      //日のデーターを作成してる
      Calendar.prototype.backFill = function() {
        var clone = this.current.clone();
        var dayOfWeek = clone.day();

        if(!dayOfWeek) { return; }

        clone.subtract('days', dayOfWeek+1);

        for(var i = dayOfWeek; i > 0 ; i--) {
          this.drawDay(clone.add('days', 1));
        }
      }

      Calendar.prototype.fowardFill = function() {
        var clone = this.current.clone().add('months', 1).subtract('days', 1);
        var dayOfWeek = clone.day();

        if(dayOfWeek === 6) { return; }

        for(var i = dayOfWeek; i < 6 ; i++) {
          this.drawDay(clone.add('days', 1));
        }
      }

      Calendar.prototype.currentMonth = function() {
        var clone = this.current.clone();

        while(clone.month() === this.current.month()) {
          this.drawDay(clone);
          clone.add('days', 1);
        }
      }

      Calendar.prototype.getWeek = function(day) {
        if(!this.week || day.day() === 0) {
          this.week = createElement('div', 'week');
          this.month.appendChild(this.week);
        }
      }

      //日を作成してるのか？表示までしてる？
      Calendar.prototype.drawDay = function(day) {
        var self = this;
        this.getWeek(day);

        //日をクリックイベント表示
        //Outer Day
        var outer = createElement('div', this.getDayClass(day));
        outer.addEventListener('click', function() {
          self.openDay(this);
        });

        //Day Name（createElementで要素を作成）
        //div要素を生成、day-nameという名前で？
        var name = createElement('div', 'day-name', day.format('ddd'));

        //Day Number
        var number = createElement('div', 'day-number', day.format('DD'));


        //Events
        var events = createElement('div', 'day-events');
        this.drawEvents(day, events);

        outer.appendChild(name);
        outer.appendChild(number);
        outer.appendChild(events);
        this.week.appendChild(outer);
      }

      //イベント表示？
      Calendar.prototype.drawEvents = function(day, element) {
        if(day.month() === this.current.month()) {
          var todaysEvents = this.events.reduce(function(memo, ev) {
            if(ev.date.isSame(day, 'day')) {
              memo.push(ev);
            }
            return memo;
          }, []);
          //イベントの色
          todaysEvents.forEach(function(ev) {
            var evSpan = createElement('span', ev.color);
            element.appendChild(evSpan);
          });
        }
      }

      //日の分類？
      Calendar.prototype.getDayClass = function(day) {
        classes = ['day'];
        if(day.month() !== this.current.month()) {
          classes.push('other');
        } else if (today.isSame(day, 'day')) {
          classes.push('today');
        }
        return classes.join(' ');
      }

      //htmlに判定
      Calendar.prototype.openDay = function(el) {
        var details, arrow;
        var dayNumber = +el.querySelectorAll('.day-number')[0].innerText || +el.querySelectorAll('.day-number')[0].textContent;
        var day = this.current.clone().date(dayNumber);

        var currentOpened = document.querySelector('.details');

        //Check to see if there is an open detais box on the current row
        if(currentOpened && currentOpened.parentNode === el.parentNode) {
          details = currentOpened;
          arrow = document.querySelector('.arrow');
        } else {
          //Close the open events on differnt week row
          //currentOpened && currentOpened.parentNode.removeChild(currentOpened);
          if(currentOpened) {
            currentOpened.addEventListener('webkitAnimationEnd', function() {
              currentOpened.parentNode.removeChild(currentOpened);
            });
            currentOpened.addEventListener('oanimationend', function() {
              currentOpened.parentNode.removeChild(currentOpened);
            });
            currentOpened.addEventListener('msAnimationEnd', function() {
              currentOpened.parentNode.removeChild(currentOpened);
            });
            currentOpened.addEventListener('animationend', function() {
              currentOpened.parentNode.removeChild(currentOpened);
            });
            currentOpened.className = 'details out';
          }

          //Create the Details Container
          details = createElement('div', 'details in');

          //Create the arrow
          var arrow = createElement('div', 'arrow');

          //Create the event wrapper

          details.appendChild(arrow);
          el.parentNode.appendChild(details);
        }

        var todaysEvents = this.events.reduce(function(memo, ev) {
          if(ev.date.isSame(day, 'day')) {
            memo.push(ev);
          }
          return memo;
        }, []);

        this.renderEvents(todaysEvents, details);

        arrow.style.left = el.offsetLeft - el.parentNode.offsetLeft + 27 + 'px';
      }

      Calendar.prototype.renderEvents = function(events, ele) {
        //Remove any events in the current details element
        var currentWrapper = ele.querySelector('.events');
        var wrapper = createElement('div', 'events in' + (currentWrapper ? ' new' : ''));

        //イベント処理
        events.forEach(function(ev) {
          var div = createElement('div', 'event');
          var square = createElement('div', 'event-category ' + ev.color);
          var span = createElement('span', '', ev.eventName);

          div.appendChild(square);
          div.appendChild(span);
          wrapper.appendChild(div);
        });

        //イベントなしを表示
        if(!events.length) {
          var div = createElement('div', 'event empty');
          var span = createElement('span', '', 'No Events');

          div.appendChild(span);
          wrapper.appendChild(div);
        }

        if(currentWrapper) {
          currentWrapper.className = 'events out';
          currentWrapper.addEventListener('webkitAnimationEnd', function() {
            currentWrapper.parentNode.removeChild(currentWrapper);
            ele.appendChild(wrapper);
          });
          currentWrapper.addEventListener('oanimationend', function() {
            currentWrapper.parentNode.removeChild(currentWrapper);
            ele.appendChild(wrapper);
          });
          currentWrapper.addEventListener('msAnimationEnd', function() {
            currentWrapper.parentNode.removeChild(currentWrapper);
            ele.appendChild(wrapper);
          });
          currentWrapper.addEventListener('animationend', function() {
            currentWrapper.parentNode.removeChild(currentWrapper);
            ele.appendChild(wrapper);
          });
        } else {
          ele.appendChild(wrapper);
        }


      }

      Calendar.prototype.nextMonth = function() {
        this.current.add('months', 1);
        this.draw();
      }

      Calendar.prototype.prevMonth = function() {
        this.current.subtract('months', 1);
        this.draw();
      }

      window.Calendar = Calendar;

      function createElement(tagName, className, innerText) {
        var ele = document.createElement(tagName);
        if(className) {
          ele.className = className;
        }
        if(innerText) {
          ele.innderText = ele.textContent = innerText;
        }
        return ele;
      }
    }();

    //dbにある分だけ配列を増やしたい
    //イベントとかのデータ
    !function() {
      var test = JSON.parse('<?php echo $test ?>');
      var data = [
        { eventName: test[0], calendar: 'Work', color: 'orange', day: test[1] },
            ];
/*
      test.forEach(function(day)){
        data = [
          { eventName: day[0], calendar: 'Work', color: 'orange', day: day[1] },
              ];
      }
*/
      //配列の各要素を日に割り当て
      data.forEach(function(ev) {
        addDate(ev);
      });

      //forで入れている
      function addDate(ev) {
        ev.date = moment().date(data[0]['day']);
        //ev.date = moment().date(Math.random() * (29 - 1) + 1);
      }

      //calendaridとデータを
      var calendar = new Calendar('#calendar', data);
      //window.setInterval(function() {
        //calendar.nextMonth();
      //}, 3000);
      //calendar.nextMonth();
    }();


    </script>

</body>