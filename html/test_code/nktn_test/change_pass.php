<?php

session_start();
error_reporting( E_ERROR | E_WARNING | E_PARSE );


// エラーメッセージ
$errorMessage = "";

//セッションからデータ受け取り
$id = $_SESSION[ "id" ];
$pass = $_SESSION[ "pass" ];

//データベース接続
$link = mysqli_connect( "localhost", "daisuke", "miyagawa", "PBLD" );
if ( !$link ) {
	print( mysqli_connect_error( $link ) );

	exit();
}

// エスケープする
$opass = htmlspecialchars( $_POST[ "opass" ], ENT_QUOTES );
$npass = htmlspecialchars( $_POST[ "npass" ], ENT_QUOTES );
$rpass = htmlspecialchars( $_POST[ "rpass" ], ENT_QUOTES );

if ( isset( $_POST[ "button" ] ) ) {

	//押されたボタンの判定
	$kbn = htmlspecialchars( $_POST[ "button" ], ENT_QUOTES, "UTF-8" );
	switch ( $kbn ) {
		case "enter":
			if ( !preg_match( "/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,10}+\z/i", $npass ) ) {
				$errorMessage = "入力されたパスワードが適当すぎます。";
				break;
			}
			if ( preg_match( '/((.+)\2{3})\2*/i', $npass ) || check( $npass ) ) {
				$errorMessage = "同じ文字または文字列が連続で使用されています。";
				break;
			}


			if ( $npass != $rpass ) {
				$errorMessage = "再入力されたパスワードが間違っています";
				break;
			}

			//passの更新
			$result = mysqli_query( $link, "UPDATE user SET pass = '$npass' WHERE id = '$id' AND pass = '$opass'" );
			if ( !$result ) {
				print( mysqli_error( $link ) );
			} else {
				$_SESSION[ "id" ] = $id;
				$_SESSION[ "pass" ] = $npass;
			}
			//リダイレクト
			if ( $id == 'teacher' ) {
				header( "Location: home_teacher.php" );
			} else {
				header( "Location: home_student.php" );
			}
			exit();
			break;
		case "cancel":
			//リダイレクト
			if ( $id ==
				'teacher' ) {
				header( "Location: home_teacher.php" );
			} else {
				header( "Location: home_student.php" );
			}
			exit();
			break;
	}
}

//SELECT文
$result = mysqli_query( $link, "SELECT * FROM user WHERE id = '$id' AND pass ='$pass'" );
if ( !$result ) {
	print( mysqli_error( $link ) );
	exit();
}

$row = mysqli_fetch_assoc( $result );

if ( $row[ id ] == null )echo 'ログインできませんでした。';

//アルファベット順をチェック
function check( $str ) {

	$k = 0;

	for ( $i = 0; $i <= strlen( $str ); $i++ ) {

		//次の文字を取得する処理
		$char = $str[ $i ];
		$char2 = ++$char;

		if ( $str[ $i + 1 ] == $char2 ) {
			$k++;
		} else $k = 0;

		//アルファベット順が４つ揃ったら
		if ( $k >= 3 ) return true;

	}
	return false;
}

?>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>パスワード変更</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/login.css" media="all"/>
</head>

<body>
	<div class="page-header" style="margin-top: 20px;">
		<br>
		<h2 class="text-center">パスワード変更</h2>
	</div>

	<div>
		<?php echo $errorMessage ?>
	</div>

	<div class="container">
	 <div class="row main">
	
	  <div class="form-header">
	   <h1 class="text-center">Change Pass</h1>
	  </div>
	
	<div class="loginform">
		<form action="" method="POST">
			<p>使用中のパスワード</p>
			<p class="pass"><input type="input" class="form-control" name="opass"/>
			</p>
			<p>新しいパスワード</p>
			<p class="pass"><input type="password" class="form-control" name="npass"/>
			</p>
			<p>新しいパスワードを確認</p>
			<p class="pass"><input type="password" class="form-control" name="rpass"/>
			</p>
			<div Align="right">
				<button type="submit" value="enter" name="button" class="btn btn-default">Enter</button>
				<button type="submit" value="cancel" name="button" class="btn btn-default">Cancel</button>
			</div>
	  </form>
	</div>
	
	 </div>
	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>

</html>