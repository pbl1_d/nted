// !function() {
//       //オリジナル関数なのか既存の関数なのか
//       //外部の使ってる（moment.js）
//       var today = moment();
//
//       //表示する場所（idで指定）、イベントのデータが引数
//       function Calendar(selector, events) {
//         this.el = document.querySelector(selector);
//         this.events = events;
//         this.current = moment().date(1);
//         this.draw();
//         //今日作成
//         //todayを取得
//         //クラスにtodayを持つ要素を
//         var current = document.querySelector('.today');
//         if(current) {
//           var self = this;
//           //500ミリ秒後にopenDayを実行
//           window.setTimeout(function() {
//             self.openDay(current);
//           }, 500);
//         }
//
//       }
//
//       //ヘッダーと月を取り敢えず生成
//       //this.draw()
//       Calendar.prototype.draw = function() {
//         //Create Header
//         this.drawHeader();
//
//         //Draw Month
//         this.drawMonth();
//       }
//
//     　//ヘッダーの具体的な作成
//       Calendar.prototype.drawHeader = function() {
//         var self = this;
//         if(!this.header) {
//           //headerを作成
//           this.header = createElement('div', 'header');
//           this.header.className = 'header';
//
//           this.title = createElement('h1');
//
//           //押して月の切り替え
//           var right = createElement('div', 'right');
//           //rightをクリックすると
//           right.addEventListener('click', function() { self.nextMonth(); });
//
//           var left = createElement('div', 'left');
//           left.addEventListener('click', function() { self.prevMonth(); });
//
//           //appendChildは更新
//           //Append the Elements
//           //ヘッダーに追加
//           this.header.appendChild(this.title);
//           this.header.appendChild(right);
//           this.header.appendChild(left);
//           this.el.appendChild(this.header);
//         }
//         //月、年の表示
//         //innerHTMLはテキスト
//         this.title.innerHTML = this.current.format('MMMM YYYY');
//       }
//
//       //月を作成
//       Calendar.prototype.drawMonth = function() {
//         if(!this.month) {
//           this.month = createElement('div', 'month');
//           this.el.appendChild(this.month);
//         }
//
//         this.month.innerHTML = '';
//         this.backFill();
//         this.currentMonth();
//         this.fowardFill();
//       }
//
//       //日のデーターを作成してる
//       Calendar.prototype.backFill = function() {
//         var clone = this.current.clone();
//         var dayOfWeek = clone.day();
//
//         if(!dayOfWeek) { return; }
//
//         clone.subtract('days', dayOfWeek+1);
//
//         for(var i = dayOfWeek; i > 0 ; i--) {
//           this.drawDay(clone.add('days', 1));
//         }
//       }
//
//       Calendar.prototype.fowardFill = function() {
//         var clone = this.current.clone().add('months', 1).subtract('days', 1);
//         var dayOfWeek = clone.day();
//
//         if(dayOfWeek === 6) { return; }
//
//         for(var i = dayOfWeek; i < 6 ; i++) {
//           this.drawDay(clone.add('days', 1));
//         }
//       }
//
//       Calendar.prototype.currentMonth = function() {
//         var clone = this.current.clone();
//
//         while(clone.month() === this.current.month()) {
//           this.drawDay(clone);
//           clone.add('days', 1);
//         }
//       }
//
//       Calendar.prototype.getWeek = function(day) {
//         if(!this.week || day.day() === 0) {
//           this.week = createElement('div', 'week');
//           this.month.appendChild(this.week);
//         }
//       }
//
//       //日を作成してる、表示までしてる
//       Calendar.prototype.drawDay = function(day) {
//         var self = this;
//         this.getWeek(day);
//
//         //日をクリックイベント表示
//         //Outer Day
//         var outer = createElement('div', this.getDayClass(day));
//         //クリックしたらイベントを開く
//         outer.addEventListener('click', function() {
//           self.openDay(this);
//         });
//
//         //createElementで要素を作成
//         //div要素を生成、day-nameという名前で
//         var name = createElement('div', 'day-name', day.format('ddd'));
//
//         //Day Number
//         var number = createElement('div', 'day-number', day.format('DD'));
//
//         //Events
//         var events = createElement('div', 'day-events');
//         this.drawEvents(day, events);
//
//         outer.appendChild(name);
//         outer.appendChild(number);
//         outer.appendChild(events);
//         this.week.appendChild(outer);
//       }
//
//       //イベント表示
//       Calendar.prototype.drawEvents = function(day, element) {
//         if(day.month() === this.current.month()) {
//           var todaysEvents = this.events.reduce(function(memo, ev) {
//             if(ev.date.isSame(day, 'day')) {
//               memo.push(ev);
//             }
//             return memo;
//           }, []);
//           //イベントの色
//           todaysEvents.forEach(function(ev) {
//             var evSpan = createElement('span', ev.color);
//             element.appendChild(evSpan);
//           });
//         }
//       }
//
//       //日の分類
//       Calendar.prototype.getDayClass = function(day) {
//         classes = ['day'];
//         if(day.month() !== this.current.month()) {
//           classes.push('other');
//         } else if (today.isSame(day, 'day')) {
//           classes.push('today');
//         }
//         return classes.join(' ');
//       }
//
//       //htmlに反映
//       Calendar.prototype.openDay = function(el) {
//         var details, arrow;
//         var dayNumber = +el.querySelectorAll('.day-number')[0].innerText || +el.querySelectorAll('.day-number')[0].textContent;
//         var day = this.current.clone().date(dayNumber);
//
//         var currentOpened = document.querySelector('.details');
//
//         //Check to see if there is an open detais box on the current row
//         if(currentOpened && currentOpened.parentNode === el.parentNode) {
//           details = currentOpened;
//           arrow = document.querySelector('.arrow');
//         } else {
//           //Close the open events on differnt week row
//           //currentOpened && currentOpened.parentNode.removeChild(currentOpened);
//           if(currentOpened) {
//             currentOpened.addEventListener('webkitAnimationEnd', function() {
//               currentOpened.parentNode.removeChild(currentOpened);
//             });
//             currentOpened.addEventListener('oanimationend', function() {
//               currentOpened.parentNode.removeChild(currentOpened);
//             });
//             currentOpened.addEventListener('msAnimationEnd', function() {
//               currentOpened.parentNode.removeChild(currentOpened);
//             });
//             currentOpened.addEventListener('animationend', function() {
//               currentOpened.parentNode.removeChild(currentOpened);
//             });
//             currentOpened.className = 'details out';
//           }
//
//           //Create the Details Container
//           details = createElement('div', 'details in');
//
//           //Create the arrow
//           var arrow = createElement('div', 'arrow');
//
//           //Create the event wrapper
//
//           details.appendChild(arrow);
//           el.parentNode.appendChild(details);
//         }
//
//         var todaysEvents = this.events.reduce(function(memo, ev) {
//           if(ev.date.isSame(day, 'day')) {
//             memo.push(ev);
//           }
//           return memo;
//         }, []);
//
//         this.renderEvents(todaysEvents, details);
//
//         arrow.style.left = el.offsetLeft - el.parentNode.offsetLeft + 27 + 'px';
//       }
//
//       Calendar.prototype.renderEvents = function(events, ele) {
//         //Remove any events in the current details element
//         var currentWrapper = ele.querySelector('.events');
//         var wrapper = createElement('div', 'events in' + (currentWrapper ? ' new' : ''));
//
//         //イベントあり処理
//         events.forEach(function(ev) {
//           var div = createElement('div', 'event');
//           var square = createElement('div', 'event-category ' + ev.color);
//           var span = createElement('span', '', ev.eventName);
//
//           div.appendChild(square);
//           div.appendChild(span);
//           wrapper.appendChild(div);
//         });
//
//         //イベントなしを表示
//         if(!events.length) {
//           var div = createElement('div', 'event empty');
//           //span要素No Eventsテキスト
//           var span = createElement('span', '', 'No Events');
//
//           /*ボタンを
//           var form = createElement('form');
//           form.method = "post";
//           var input = createElement('button');
//           input.type = "submit";
//           input.className = "btn btn-default";
//           input.innerHTML = "log";
//
//           //divにspan
//           form.appendChild(input);
//           span.appendChild(form);
//           */
//
//           div.appendChild(span);
//           wrapper.appendChild(div);
//         }
//
//         if(currentWrapper) {
//           currentWrapper.className = 'events out';
//           currentWrapper.addEventListener('webkitAnimationEnd', function() {
//             currentWrapper.parentNode.removeChild(currentWrapper);
//             ele.appendChild(wrapper);
//           });
//           currentWrapper.addEventListener('oanimationend', function() {
//             currentWrapper.parentNode.removeChild(currentWrapper);
//             ele.appendChild(wrapper);
//           });
//           currentWrapper.addEventListener('msAnimationEnd', function() {
//             currentWrapper.parentNode.removeChild(currentWrapper);
//             ele.appendChild(wrapper);
//           });
//           currentWrapper.addEventListener('animationend', function() {
//             currentWrapper.parentNode.removeChild(currentWrapper);
//             ele.appendChild(wrapper);
//           });
//         } else {
//           ele.appendChild(wrapper);
//         }
//
//
//       }
//
//       Calendar.prototype.nextMonth = function() {
//         this.current.add('months', 1);
//         this.draw();
//       }
//
//       Calendar.prototype.prevMonth = function() {
//         this.current.subtract('months', 1);
//         this.draw();
//       }
//
//       window.Calendar = Calendar;
//
//       function createElement(tagName, className, innerText) {
//         var ele = document.createElement(tagName);
//         if(className) {
//           ele.className = className;
//         }
//         if(innerText) {
//           ele.innderText = ele.textContent = innerText;
//         }
//         return ele;
//       }
//     }();
//
//     //dbにある分だけ配列を増やしたい
//     //イベントとかのデータ
//     !function() {
//       var data = [
//             // eventName: 'test', calendar: 'Work', color: 'orange', day: '2' }
//             ];
//
//       //今月
//       var month = moment().month();
//       //document.write(month);
//
//       for(var i=0; i<ev_data.length; i++){
//         //今月のイベントか判定
//         if(month == ev_data[i][0].slice(5,7)){
//         data.push({ eventName: ev_data[i][1], calendar: 'Work', color: 'orange', day: ev_data[i][0].slice(8)}) ;
//        }
//       }
//
//   //     !function() {
//   // var data = [
//   //   { eventName: 'Lunch Meeting w/ Mark', calendar: 'Work', color: 'orange' },
//   //   { eventName: 'Ice Cream Night', calendar: 'Kids', color: 'yellow' },
//   // ];
//
//       //配列の各要素を日に割り当て
//       data.forEach(function(ev) {
//         addDate(ev);
//       });
//
//       //forで入れている
//       function addDate(ev) {
//         ev.date = moment().date(ev.day);
//         //ev.date = moment().date(Math.random() * (29 - 1) + 1);
//       }
//
//       //calendaridとデータを
//       var calendar = new Calendar('#calendar', data);
//
//       //window.setInterval(function() {
//         //calendar.nextMonth();
//       //}, 3000);
//       //calendar.nextMonth();
//     }();


!function() {

  var today = moment();

  function Calendar(selector, events) {
    this.el = document.querySelector(selector);
    this.events = events;
    this.current = moment().date(1);
    this.draw();
    var current = document.querySelector('.today');
    if(current) {
      var self = this;
      window.setTimeout(function() {
        self.openDay(current);
      }, 500);
    }
  }

  Calendar.prototype.draw = function() {
    //Create Header
    this.drawHeader();

    //Draw Month
    this.drawMonth();
  }

  Calendar.prototype.drawHeader = function() {
    var self = this;
    if(!this.header) {
      //Create the header elements
      this.header = createElement('div', 'header');
      this.header.className = 'header';

      this.title = createElement('h1');

      var right = createElement('div', 'right');
      right.addEventListener('click', function() { self.nextMonth(); });

      var left = createElement('div', 'left');
      left.addEventListener('click', function() { self.prevMonth(); });

      //Append the Elements
      this.header.appendChild(this.title);
      this.header.appendChild(right);
      this.header.appendChild(left);
      this.el.appendChild(this.header);
    }

    this.title.innerHTML = this.current.format('MMMM YYYY');
  }

  Calendar.prototype.drawMonth = function() {
    if(!this.month) {
      this.month = createElement('div', 'month');
      this.el.appendChild(this.month);
    }

    this.month.innerHTML = '';
    this.backFill();
    this.currentMonth();
    this.fowardFill();
  }

  Calendar.prototype.backFill = function() {
    var clone = this.current.clone();
    var dayOfWeek = clone.day();

    if(!dayOfWeek) { return; }

    clone.subtract('days', dayOfWeek+1);

    for(var i = dayOfWeek; i > 0 ; i--) {
      this.drawDay(clone.add('days', 1));
    }
  }

  Calendar.prototype.fowardFill = function() {
    var clone = this.current.clone().add('months', 1).subtract('days', 1);
    var dayOfWeek = clone.day();

    if(dayOfWeek === 6) { return; }

    for(var i = dayOfWeek; i < 6 ; i++) {
      this.drawDay(clone.add('days', 1));
    }
  }

  Calendar.prototype.currentMonth = function() {
    var clone = this.current.clone();

    while(clone.month() === this.current.month()) {
      this.drawDay(clone);
      clone.add('days', 1);
    }
  }

  Calendar.prototype.getWeek = function(day) {
    if(!this.week || day.day() === 0) {
      this.week = createElement('div', 'week');
      this.month.appendChild(this.week);
    }
  }

  Calendar.prototype.drawDay = function(day) {
    var self = this;
    this.getWeek(day);

    //Outer Day
    var outer = createElement('div', this.getDayClass(day));
    outer.addEventListener('click', function() {
      self.openDay(this);

    });

    //Day Name
    var name = createElement('div', 'day-name', day.format('ddd'));

    //Day Number
    var number = createElement('div', 'day-number', day.format('DD'));


    //Events
    var events = createElement('div', 'day-events');
    this.drawEvents(day, events);

    outer.appendChild(name);
    outer.appendChild(number);
    outer.appendChild(events);
    this.week.appendChild(outer);
  }

  Calendar.prototype.drawEvents = function(day, element) {
    if(day.month() === this.current.month()) {
      var todaysEvents = this.events.reduce(function(memo, ev) {
        if(ev.date.isSame(day, 'day')) {
          memo.push(ev);
        }
        return memo;
      }, []);

      todaysEvents.forEach(function(ev) {
        var evSpan = createElement('span', ev.color);
        element.appendChild(evSpan);
      });
    }
  }

  Calendar.prototype.getDayClass = function(day) {
    classes = ['day'];
    if(day.month() !== this.current.month()) {
      classes.push('other');
    } else if (today.isSame(day, 'day')) {
      classes.push('today');
    }
    return classes.join(' ');
  }

  Calendar.prototype.openDay = function(el) {
    var details, arrow;
    var dayNumber = +el.querySelectorAll('.day-number')[0].innerText || +el.querySelectorAll('.day-number')[0].textContent;
    var day = this.current.clone().date(dayNumber);

    var currentOpened = document.querySelector('.details');

    //Check to see if there is an open detais box on the current row
    if(currentOpened && currentOpened.parentNode === el.parentNode) {
      details = currentOpened;
      arrow = document.querySelector('.arrow');
    } else {
      //Close the open events on differnt week row
      //currentOpened && currentOpened.parentNode.removeChild(currentOpened);
      if(currentOpened) {
        currentOpened.addEventListener('webkitAnimationEnd', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('oanimationend', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('msAnimationEnd', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('animationend', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.className = 'details out';
      }

      //Create the Details Container
      details = createElement('div', 'details in');

      //Create the arrow
      var arrow = createElement('div', 'arrow');

      //Create the event wrapper

      details.appendChild(arrow);
      el.parentNode.appendChild(details);
    }

    var todaysEvents = this.events.reduce(function(memo, ev) {
      if(ev.date.isSame(day, 'day')) {
        memo.push(ev);
      }
      return memo;
    }, []);

    this.renderEvents(todaysEvents, details);

    arrow.style.left = el.offsetLeft - el.parentNode.offsetLeft + 27 + 'px';
  }

  Calendar.prototype.renderEvents = function(events, ele) {
    //Remove any events in the current details element
    var currentWrapper = ele.querySelector('.events');
    var wrapper = createElement('div', 'events in' + (currentWrapper ? ' new' : ''));

    events.forEach(function(ev) {
      var div = createElement('div', 'event');
      var square = createElement('div', 'event-category ' + ev.color);
      var span = createElement('span', '', ev.eventName);

      div.appendChild(square);
      div.appendChild(span);
      wrapper.appendChild(div);
    });

    if(!events.length) {
      var div = createElement('div', 'event empty');
      var span = createElement('span', '', 'No Events');

      div.appendChild(span);
      wrapper.appendChild(div);
    }

    if(currentWrapper) {
      currentWrapper.className = 'events out';
      currentWrapper.addEventListener('webkitAnimationEnd', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('oanimationend', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('msAnimationEnd', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('animationend', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
    } else {
      ele.appendChild(wrapper);
    }


  }

  Calendar.prototype.nextMonth = function() {
    this.current.add('months', 1);
    this.draw();
  }

  Calendar.prototype.prevMonth = function() {
    this.current.subtract('months', 1);
    this.draw();
  }

  window.Calendar = Calendar;

  function createElement(tagName, className, innerText) {
    var ele = document.createElement(tagName);
    if(className) {
      ele.className = className;
    }
    if(innerText) {
      ele.innderText = ele.textContent = innerText;
    }
    return ele;
  }
}();

!function() {
  var go= "出席";
  var data = [
    { eventName: '1限目:'+go , calendar: 'Work', color: 'orange' },
    { eventName: 'Interview - Jr. Web Developer', calendar: 'Work', color: 'orange' },
  ];

  data.forEach(function(ev) {
    addDate(ev);
  });

  function addDate(ev) {
    var i=1;
    for(i;i<5;i++){
    ev.date = moment().date(i);
    // ev.date=moment().date(1);
    }
  }

  var calendar = new Calendar('#calendar', data);
  //window.setInterval(function() {
    //calendar.nextMonth();
  //}, 3000);
  //calendar.nextMonth();
}();
