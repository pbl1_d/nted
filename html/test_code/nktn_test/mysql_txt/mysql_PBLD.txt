# 文字コードのセット
set names utf8;
# データベース PBLD の作成
drop database if exists PBLD;
create database PBLD;
# ユーザー「daisuke」にパスワード「miyagawa」を設定し、データベース「PBLD」に対するすべての権限を付与
grant all privileges on PBLD.* to daisuke@localhost identified by 'miyagawa';
set foreign_key_checks = 0;
#データベース PBLD を使用
use PBLD;
# テーブルuser の作成
create table user(id varchar(7) primary key,
name varchar(20),
pass varchar(10)

);

# テーブルtoken の作成
create table token(id varchar(7) primary key,
token varchar(60),
cookietime int
);


create table cal (day DATE primary key,
youbi varchar(1),
yasumi int(1)
);

create table kyujitu(holiday DATE primary key,
reasonh varchar(50)
);

# テーブル  att の作成
create table att(id varchar(7) ,
foreign key(id) references user(id),
day DATE,
foreign key(day) references cal(day),
gotime time,
backtime time,
per1 int(1),
per2 int(1),
per3 int(1),
per4 int(1),
per5 int(1),
reason varchar(50)
)engine=InnoDB;
