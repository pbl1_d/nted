<?php
// //外部オブジェクト
// require "class/database.php";
//
// $data = new connect;
// //イベントのデータを取得
// $ev_data = $data->event();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ホーム</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/calen.css">
    <!-- <link href="css/button2.css" rel="stylesheet"> -->

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/calendar2.js"></script>

    <script>
     $(document).ready(function () {
       $('.button-1').clickSpark();

    //   $('.button-1').click(function(){
    //   // $('.button-1').clickSpark();
    //   $.ajax({
    //     type: "POST",
    //     url: "",
    //     data: {data1:'aaa',data2:'bbb'},
    //     success: function() {
    //       // alert("sox murai");
    //
    //     }
    //   });
    // });
    });

    </script>

    <script>
      //phpのデータを受け取る
      var ev_data = '<?php echo $ev_data; ?>';
      var ev_data = JSON.parse(ev_data);
    </script>

</head>

<body>

    <!--ヘッダ-->
    <div class="page-header container">
        <h1 align="center">出席管理システム</h1>
    </div>

    <!-- <p><a href="./change_pass.php">パスワード変更</a></p> -->
</body>

</html>

<?php
//セッション開始
date_default_timezone_set('Japan');  //タイムゾーン設定(日本)
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);



//最終アクセス時間からセッション管理
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3900)) {
// 最終リクエスト時刻から65分経過した
session_unset();     //
session_destroy();   // セッション破棄
}
$_SESSION['LAST_ACTIVITY'] = time(); // 最終リクエスト時刻を更新
$_SESSION['last_log']= date('Y/m/d H:i:s');


//データベース接続
$link = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
if (!$link) {
    print(mysqli_connect_error($link));
    exit();
}



//出席ボタンが押された場合の呼び出し
if(isset($_POST["att"])){
  if(!isset($_COOKIE["att"])&&isset($_SESSION["id"])){
  sleep(3);
  include("./stu.php");
}else if(!isset($_SESSION["id"])){

  //処理なし
  sleep(3);

}else{
  sleep(3);
  echo '<span style="color:#322EF2"><h1><b><center>既に出席済みです。</center></b></h1></span>';

}

}

if(isset($_POST["logout"])){
    session_destroy();
    setcookie('save', 'off', time() + 60 * 60 * 24 * 14, '/');
    header('Location: ./login.php');
    exit();
}

//セッションからデータ受け取り
$id = $_SESSION["id"];
$pass = $_SESSION["pass"];
// $save = $_SESSION['save'];

// //cookieに保存
// if($save == 'on'){
//     setcookie('my_id', $id, time() + 60 * 60 * 24 * 14);
//     setcookie('my_pass', $pass, time() + 60 * 60 * 24 * 14);
//
// }else{
//     setcookie('my_id','');
//     setcookie('my_pass','');
// }

//SELECT文
$result = mysqli_query($link,"SELECT * FROM user WHERE id = '$id' AND pass ='$pass'");
if (!$result) {
    print(mysqli_error($link));
    exit();
}

$row = mysqli_fetch_assoc($result);


//ログイン失敗の処理
if($row[id] == null){
  session_destroy();
  echo 'ログインできませんでした。';
  echo '
  <form action="" method="POST">
  <button type="submit" value="buck" name="button" class="btn btn-default btn-lg">戻る</button>
  </form>
  ';
  $but = htmlspecialchars($_POST["button"], ENT_QUOTES, "UTF-8");
  switch ($but) {
      case "buck":
          session_destroy();
          // setcookie('save', 'off', time() + 60 * 60 * 24 * 14, '/');
          header("Location: login.php");
          break;
  }


//ログイン許可時の処理
}else{
  //初期パスワードの場合
  if($id==$pass){
                      $_SESSION["id"]   = $id;
                $_SESSION["pass"] = $pass;

    header('Location: ./change_pass.php');
    exit;
  }


    echo '
        <!--ヘッダ-->
        <div class="container">
          <div class="page-header">
            <div align="right">
              <form action="" method="POST">
              <h3>
                  '. $row[id] .' '. $row[name] .'
                  <button type="submit" name="logout" class="btn btn-default">Logout</button><br />
              </h3>
              最終ログイン:'. $_SESSION[last_log] .'
              </form>

              <form action="" method="POST">
              <button type="submit" name="att" class="btn btn-info button-1" id="btn">授業に出席します</button>
              </form>

            </div>
          </div>
          <script src="js/clickspark.js"></script>
        <!--メインコンテンツ-->

        <!--タブ一覧-->

        <div class="col-sm-2">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="active"><a href="#menu1" data-toggle="tab">ホーム</a></li>
                <li role="presentation"><a href="#menu2" data-toggle="tab">出席状況確認(詳細)</a></li>
                <li role="presentation"><a href="#menu3" data-toggle="tab">パスワード変更</a></li>
            </ul>
        </div>

    ';

    echo '

        <!--選択タブ内のコンテンツ-->
        <div class="col-sm-8 tab-content">
            <div class="tab-pane active" id="menu1">

                <!--出席状況の簡易確認-->
                <h3>本日の出席状況</h3>
                <table class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>時間</th>
                            <th>出欠</th>
                            <th>欠席理由</th>
                        </tr>
                    </thead>
                    <tbody>
    ';

    // 結果セットを開放します
    mysqli_free_result($result);

    //SELECT文
    $result = mysqli_query($link,"SELECT * FROM att WHERE id = '$id'");
    if (!$result) {
        print(mysqli_error($link));
        exit();
    }

    $row = mysqli_fetch_assoc($result);

    for($i = 0; $i < 5; $i++){
        echo '<tr>';
        echo '<th>'. ($i + 1) .'限</th>';
        echo '<td>';
        switch($row[per.($i + 1)]){
            case 0:
                echo '出席';
                break;
            case 1:
                echo '遅刻';
                break;
            case 2:
                echo '欠席';
                break;
            case 3:
                echo '就活';
                break;
            case 4:
                echo '病欠';
                break;
            case 5:
                echo '公欠';
                break;

        }
        echo '</td>';
        if($i == 0) echo '<td rowspan="5">'. $row[reason] .'</td>';
        echo '</tr>';
    };

// $test='<script type="text/javascript" src="js/calendar2.js">document.getElementById("day-view-date");</script>';
//
// $test1=1;
    echo '
                    </tbody>
                </table>
            </div>

            <div class="tab-pane" id="menu2">
            <div class="calendar" id="calendar-app">
<div class="calendar--day-view" id="day-view">
  <span class="day-view-exit" id="day-view-exit">&times;</span>
  <span class="day-view-date" id="day-view-date">MAY 29 2016</span>
  <div class="day-view-content">
    <div class="day-highlight">
      You <span class="day-events" naem="test" id="day-events">had no events for today</span>. &nbsp; <span tabindex="0" onkeyup="if(event.keyCode != 13) return; this.click();" class="day-events-link" id="add-event" data-date>Add a new event?</span>

    </div>
    <div class="day-add-event" id="add-day-event-box" data-active="false">
      <div class="row">
        <div class="half">
          <label class="add-event-label">
             Name of event
            <input type="text" class="add-event-edit add-event-edit--long" placeholder="New event" id="input-add-event-name">

          </label>
        </div>
        <div class="qtr">
          <label class="add-event-label">
        Start Time
            <input type="text" class="add-event-edit" placeholder="8:15" id="input-add-event-start-time" data-options="1,2,3,4,5,6,7,8,9,10,11,12" data-format="datetime">
            <input type="text" class="add-event-edit" placeholder="am" id="input-add-event-start-ampm" data-options="a,p,am,pm">
          </label>
        </div>
        <div class="qtr">
          <label class="add-event-label">
        End Time
            <input type="text" class="add-event-edit" placeholder="9" id="input-add-event-end-time" data-options="1,2,3,4,5,6,7,8,9,10,11,12" data-format="datetime">
            <input type="text" class="add-event-edit" placeholder="am" id="input-add-event-end-ampm" data-options="a,p,am,pm">
          </label>
        </div>
        <div class="half">
          <a onkeyup="if(event.keyCode != 13) return; this.click();" tabindex="0" id="add-event-save" class="event-btn--save event-btn">save</a>
          <a tabindex="0" id="add-event-cancel" class="event-btn--cancel event-btn">cancel</a>
        </div>
      </div>

    </div>
    <div id="day-events-list" class="day-events-list">

    </div>
    <div class="day-inspiration-quote" id="inspirational-quote">
      Every child is an artist.  The problem is how to remain an artist once he grows up. –Pablo Picasso
    </div>
  </div>
</div>
<div class="calendar--view" id="calendar-view">
  <div class="cview__month">
    <span class="cview__month-last" id="calendar-month-last">Apr</span>
    <span class="cview__month-current" id="calendar-month">May</span>
    <span class="cview__month-next" id="calendar-month-next">Jun</span>
  </div>
  <div class="cview__header">Sun</div>
  <div class="cview__header">Mon</div>
  <div class="cview__header">Tue</div>
  <div class="cview__header">Wed</div>
  <div class="cview__header">Thu</div>
  <div class="cview__header">Fri</div>
  <div class="cview__header">Sat</div>
  <div class="calendar--view" id="dates">
  </div>
</div>
<div class="footer">
  <span><span id="footer-date" class="footer__link"></span></span>
</div>

<script src="js/calendar2.js"></script>


</div>
            </div>

            <div class="tab-pane" id="menu3">
                <a href="change_pass.php">パス変更</a>

            </div>
        </div>
        </div>
    ';

}

?>
