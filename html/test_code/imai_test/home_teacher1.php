<?php
//外部オブジェクト
require "class/database.php";

$data = new connect;
//イベントのデータを取得
$ev_data = $data->event();
?>
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ホーム/教員用</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/calendar.css">
	<link rel="stylesheet" href="css/card.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>

	<script>
		//phpのデータを受け取る
		var ev_data = '<?php echo $ev_data; ?>';
		var ev_data = JSON.parse(ev_data);
	</script>

</head>

<body>
	<div class="container">
		<div class="page-header">
			<h1 class="text-center">学生管理システム/教員</h1>
		</div>
	</div>
</body>

</html>

<?php

//セッション開始
session_start();
error_reporting( E_ERROR | E_WARNING | E_PARSE );

//最終アクセス時間からセッション管理
if ( isset( $_SESSION[ 'LAST_ACTIVITY' ] ) && ( time() - $_SESSION[ 'LAST_ACTIVITY' ] > 3900 ) ) {
	// 最終リクエスト時刻から30分経過した
	session_unset(); //
	session_destroy(); // セッション破棄
}
$_SESSION[ 'LAST_ACTIVITY' ] = time(); // 最終リクエスト時刻を更新


//セッションからデータ受け取り
$id = $_SESSION[ "id" ];
$pass = $_SESSION[ "pass" ];
// $save = $_SESSION['save'];
//
// //cookieに保存
// if($save == 'on'){
//     setcookie('my_id', $id, time() + 60 * 60 * 24 * 14);
//     setcookie('my_pass', $pass, time() + 60 * 60 * 24 * 14);
//
// }else{
//     setcookie('my_id','');
//     setcookie('my_pass','');
// }


//データベース接続
$link = mysqli_connect( "localhost", "daisuke", "miyagawa", "PBLD" );
if ( !$link ) {
	print( mysqli_connect_error( $link ) );
	exit();
}

if ( isset( $_POST[ "logout" ] ) ) {
	session_destroy();
	setcookie( 'save', 'off', time() + 60 * 60 * 24 * 14, '/' );
	header( 'Location: ./login.php' );
	exit();
}

//SELECT文
$result = mysqli_query( $link, "SELECT * FROM user WHERE id = '$id' AND pass ='$pass'" );
if ( !$result ) {
	print( mysqli_error( $link ) );
	exit();
}

$row = mysqli_fetch_assoc( $result );

//ログイン失敗の処理
if ( $row[ id ] == null ) {
	echo '
      <div class="container">
        <h3>ログインできませんでした。</h3>
          <div>
            <form action="" method="POST">
              <button type="submit" value="buck" name="button" class="btn btn-default btn-lg">戻る</button>
            </form>
          </div>
        </div>
      ';
	$but = htmlspecialchars( $_POST[ "button" ], ENT_QUOTES, "UTF-8" );
	switch ( $but ) {
		case "buck":
			session_destroy();
			setcookie( 'save', 'off', time() + 60 * 60 * 24 * 14, '/' );
			header( "Location: login.php" );
			break;
	}
	//================  ログイン失敗処理ここまで ================

	//ログイン許可時の処理
} else {

	//初期パスワードの場合
	if ( $id == $pass ) {
		header( 'Location: http://localhost:8080/change_pass.php' );
		exit;
	}

	echo '
  <div class="container">
    <div class="page-header">
      <div align="right">
        <h3>
          <form action="" method="post">
            ' . $row[ name ] . '  <!-入力ID(名前)を表示 ->
            <button type="submit" name="logout" class="btn btn-info" id="logout"><span class="glyphicon glyphicon-log-out"></span> ログアウト</button>
          </form>
        </h3>
      </div>
    </div>

  <div class=" col-sm-2">
    <ul class="nav nav-pills nav-stacked">
      <li role="presentation" class="active"><a href="#menu1" data-toggle="tab"><span class="glyphicon glyphicon-home"></span> ホーム</a></li>
      <li role="presentation"><a href="#menu2" data-toggle="tab"><span class="glyphicon glyphicon-calendar"></span> 登校日変更</a></li>
			<li role="presentation"><a href="#menu3" data-toggle="tab"><span class="glyphicon glyphicon-edit"></span> 出欠状況管理</a></li>
      <li role="presentation"><a href="#menu4" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> 新年度生徒登録</a></li>
      <li role="presentation"><a href="#menu5" data-toggle="tab"><span class="glyphicon glyphicon-wrench"></span> パスワード変更</a></li>
    </ul>
  </div>

';

$day=date('Y-m-d');
$year=date('Y');
$month=date('m');
$day2=date('d');

//変数dayを生成
if(isset($_POST["month"])){
	$year=$_POST["year"];
	$month=$_POST["month"];
	$day2=$_POST["day"];
	if($month<10){
		$day=$year."-0".$month."-".$day2;
	}else{
		$day=$year."-".$month."-".$day2;
	}
}

	echo '
<!--選択タブ内のコンテンツ-->
<div class="col-sm-10 col-md-10 tab-content">
<!- ================= ホーム(教員)/学生表示 ================= ->
    <div class="tab-pane active" id="menu1">

		<form action="" method="POST">
				<select name="year" id="year">';
				optionLoop("1950", date("Y"),date("Y"));
				 echo '
				 </select>
				 年
				<select name="month" id="month">';
				optionLoop('1', '12', date("m"));
				echo '</select>
				 月
				<select name="day" id="day">';
				optionLoop("1", "31", date("d"));
				echo '</select>
				日
				<button>日時決定</button>
			</form>

        <!--出席状況の簡易確認-->
        <h3>学生一覧</h3>
				<h4>'.$year.'年'.$month.'月'.$day2.'日の出欠状況</h4>
          <div class="row">
';

	// 結果セットを開放します
	mysqli_free_result( $result );

	//SELECT文
	//$result = mysqli_query( $link, " select user.id,user.name,att.per1,att.per2,att.per3,att.per4,att.per5,att.day,att.reason from user left outer join att on user.id = att.id where user.id regexp'^(0K)'  group by name order by id asc;" );
	//SELECT文
	//userとattを結合、生徒だけを、userにあるid、名前でグループ、idの昇順、
	$result = mysqli_query( $link, " select user.id,user.name,att.per1,att.per2,att.per3,att.per4,att.per5,att.day,att.reason from user left outer join att on user.id = att.id and att.day = '$day' where user.id regexp'^(0K)'  order by id asc;
" );
	if ( !$result ) {
		print( mysqli_error( $link ) );
		exit();
	}


	while ( $row = mysqli_fetch_assoc( $result ) ) {
		echo '
  <!- カード管理 ->
  <div class="col-xs-12 col-sm-6" style="margin-bottom: 20px;" >
    <article class="card " ontouchstart="">
      <div class="card-block">
        <h3 class="card-title"> ' . $row[ id ] . '</h3>
        <h4 class="text-muted">' . $row[ name ] . '</h4>
        <table class="table table-striped table-responsive">
          <tbody>
            <tr>
              <td>1限</td>
              <td>2限</td>
              <td>3限</td>
              <td>4限</td>
              <td>5限</td>
            </tr>
						<tr>
				 ';

				 for($i = 0; $i < 5; $i++){

		       switch($row[per.($i + 1)]){
		       case null:
		           echo '<center><b>出席データが存在しません</b></center>';
		           $low=1;
		           $i=6;
		           break;
		         case 0:
		             echo '<td>';
		             echo '出席<br>';
		             break;
		         case 1:
		             echo '<td>';
		             echo '遅刻<br>';
		             break;
		         case 2:
		             echo '<td>';
		             echo '欠席';
		             break;
		         case 3:
		             echo '<td>';
		             echo '就活';
		             break;
		         case 4:
		             echo '<td>';
		             echo '病欠';
		             break;
		         case 5:
		             echo '<td>';
		             echo '公欠';
		             break;
		         default:
		             echo 'error';
		             break;

		     }
		         echo '</td>';
		       //  if($i == 0) echo '<td rowspan="5">'. $row[reason] .'</td>';
		     };

   echo '
	 </tr>
          </tbody>
        </table>
				<p>欠席理由：' . $row[ reason ] . '</p>
      </div>
    </article>
  </div>

    ';
		echo "";
	}

	echo '
      </div>
    </div>
<!- ================= 学生表示ここまで =================->

<!- ================= 登校日管理 =================->
    <div class="tab-pane" id="menu2">
		  <div id="calendar"></div>
		  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
		  <script src="js/calendar.js"></script>
    </div>
<!- ================= 登校日管理ここまで =================->

<!- ================= 出欠状況管理 =================->
    <div class="tab-pane" id="menu3">
	 	<h3>出欠状況一覧</h3>
    </div>
<!-  ================= 出欠状況管理ここまで =================->

<!- ================= 新年度登録 =================->
    <div class="tab-pane" id="menu4">
	 	<h3>新年度生徒登録</h3>
    </div>
<!-  ================= 新年度登録ここまで =================->

<!- ================= パスワード変更 =================->
    <div class="tab-pane" id="menu5">
      <div class="container">
          <a href="change_pass.php">パス変更</a>
      </div>
    </div>
<!- ================= パスワード変更ここまで =================->
</div>
';
}
?>

<?php
//セレクトオプションのループ設定
function optionLoop($start, $end, $value = null){

	for($i = $start; $i <= $end; $i++){
		if(isset($value) &&  $value == $i){
			echo "<option value=\"{$i}\" selected=\"selected\">{$i}</option>";
		}else{
			echo "<option value=\"{$i}\">{$i}</option>";
		}
	}
}
?>
