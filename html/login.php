<?php

session_start();
error_reporting( E_ERROR | E_WARNING | E_PARSE );

//saveのcookieがからの場合
if ( !isset( $_COOKIE[ 'save' ] ) ) {
	setcookie( 'save', 'off', time() + 60 * 60 * 24 * 14, '/' );
}


//トークンと自動ログインが有効な場合
if ( isset( $_COOKIE[ 'token' ] ) && $_COOKIE[ 'save' ] == 'on' ) {
	header( "Location: ./loginche.php" );
}

//最終アクセス時間からセッション管理
if ( isset( $_SESSION[ 'LAST_ACTIVITY' ] ) && ( time() - $_SESSION[ 'LAST_ACTIVITY' ] > 10 ) ) {
	// 最終リクエスト時刻から10分経過した
	session_unset(); //
	session_destroy(); // セッション破棄
}
$_SESSION[ 'LAST_ACTIVITY' ] = time(); // 最終リクエスト時刻を更新


//ログイン回数
if ( !isset( $_SESSION[ 'loginCount' ] ) ) {
	$_SESSION[ 'loginCount' ] = 0;
}


// if($myId != $_COOKIE["my_id"] || $myPass != $_COOKIE["my_pass"]){
//   $myId="";
//   $myPass="";
//
// }


// echo'<br>';
// echo $_SESSION['loginCount'];


?>
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>出席管理ログイン</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/login.css" media="all"/>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="page-header container">
		<h1 class="text-center">出席管理システム</h1>
	</div>


	<center>
		<span class="text-danger">
			<?php echo $errorMessage ?>
		</span>
	</center>

	<?php

	// if (isset($_COOKIE['my_id']) && isset($_COOKIE['my_pass'])){
	//     $myId = $_COOKIE['my_id'];
	//     $myPass = $_COOKIE['my_pass'];
	//
	// }else{
	//     $myId = '';
	//     $myPass = '';
	// }

	if ( 5 > $_SESSION[ 'loginCount' ] ) {

		echo '

    <div id="form">
     <p class="form-title lead">Login</p>
     <form action="loginche.php" method="post">
        <p>User ID?</p>
        <p class="id"><input type="text" name="id" autocomplete="off"/></p>
        <p>Password</p>
        <p class="pass"><input type="password" name="pass" /></p>
        <div Align="right"><button type="submit" value="login" id="login" name="login" class="btn btn-default">Login</button></div>
         <label class="check"><input type="checkbox" name="save" id="save" value="off" /><label for="save" >次回から自動ログインを許可する</label>

     </form>
    </div>
  ';
	}
	?>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>

</html>