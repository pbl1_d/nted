<?php
if (is_uploaded_file($_FILES["csvfile"]["tmp_name"])) {
  $file_tmp_name = $_FILES["csvfile"]["tmp_name"];
  $file_name = $_FILES["csvfile"]["name"];

  //拡張子を判定
  if (pathinfo($file_name, PATHINFO_EXTENSION) != 'csv') {
    $err_msg = 'CSVファイルのみ対応しています。';
  } else {
    //ファイルをdataディレクトリに移動
    if (move_uploaded_file($file_tmp_name, "data/" . $file_name)) {
      //後で削除できるように権限を644に
      chmod("data/" . $file_name, 0644);
      $msg = $file_name . "をアップロードしました。";
      $file = 'data/'.$file_name;
      $fp   = fopen($file, "r");

      //データベース接続
      $link = mysqli_connect( "localhost", "daisuke", "miyagawa", "PBLD" );
      if ( !$link ) {
      	print( mysqli_connect_error( $link ) );
      	exit();
      }

      //DELETE
      $result = mysqli_query( $link, "DELETE FROM user" );
      if ( !$result ) {
        print( mysqli_error( $link ) );
        exit();
      }

      $html = "";
      //配列に変換する
      while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
        //$num = count($data);
        //$asins[] = $data;
        $html .= "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td></tr>";

        //INSERT
        $result = mysqli_query( $link, "INSERT INTO user(id, name, pass) VALUES('$data[0]', '$data[1]', '$data[2]')");
        if ( !$result ) {
          print( mysqli_error( $link ) );
          exit();
        }

      }
      fclose($fp);
      //ファイルの削除
      unlink($file);
    } else {
      $err_msg = "ファイルをアップロードできません。";
    }
  }
} else {
  $err_msg = "ファイルが選択されていません。";
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ホーム</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/calen.css"> -->
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/calendar.css">

    <!-- <link href="css/button2.css" rel="stylesheet"> -->

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/calendar.js"></script>
</head>

<body>

  <!--ヘッダ-->
  <div class="page-header container">
      <h1 align="center">出席管理システム</h1>
  </div>

  <!--エラーメッセージがあったら-->
  <?php if(isset($err_msg)) :?>
  <?php echo $err_msg;?>

<?php else : ?>
  <table class="table table-bordered table-responsive">
      <thead>
          <tr>
              <th>ID</th>
              <th>名前</th>
              <th>パスワード</th>
          </tr>
      </thead>
      <tbody>

  <?php echo $html;?>

      </tbody>
  </table>

<!--IF終了-->
<?php endif; ?>

  <a href="login.php">ログインしなおす</a>

</body>
</html>
