<?php

session_start();

//cookieの中が切れてトークンが空っぽの場合
if(!isset($_COOKIE['token'])){
  setcookie('token', '', time() + 60 * 60 * 24 * 14, '/test_code');
}
$normal_login = false;
$auto = $_COOKIE['save'];

//autoログイン処理が有効なら
if($auto=='on' && isset($_COOKIE['token'])){
  auto_login($_COOKIE['token']);
}

//idとpassに値があれば
if(isset($_POST["id"]) || isset($_POST["pass"])){
  // エスケープする
  $id = htmlspecialchars($_POST["id"], ENT_QUOTES);
  $pass = htmlspecialchars($_POST["pass"], ENT_QUOTES);
  $save = htmlspecialchars($_POST["save"], ENT_QUOTES);
  $token=$_COOKIE['token'];

  login($id, $pass, $save, $token);
}







//ログイン処理の関数
function login($id, $pass, $save, $token){
  $link = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
  if (!$link) {
     print(mysqli_connect_error($link));
     exit();
  }
  //SELECT文
  $result = mysqli_query($link,"SELECT * FROM user WHERE id = '$id' AND pass ='$pass'");
  if (!$result) {
    print(mysqli_error($link));
    exit();
  }

  $row = mysqli_fetch_assoc($result);

  if($id == $row[id] && $pass == $row[pass]) {
    // セッションIDを新規に発行する
    session_regenerate_id(TRUE);
    $_SESSION["id"]   = $id;
    $_SESSION["pass"] = $pass;
    $_SESSION["save"] = $save;
    $_SESSION['errorMessage'] = "";

    time_stamp($id);

    //token設定
    if($save=='on'){
     $date = time();
     $token = create_token();
     if(!isset($_COOKIE['token'])){
       setcookie('token', $token, time() + 60 * 60 * 24 * 7, '/');
       register_token($id,$token,$date);
     }
     setcookie('save', $save, time() + 60 * 60 * 24 * 7, '/');
    }

    if($id == 'teacher'){
      header("Location: ./home_teacher.php");
    }else if($id == null){
      session_destroy();
      setcookie('save', 'off', time() + 60 * 60 * 24 * 14, '/');
      header("Location: ./login.php");
    }else{
      header("Location: ./home_student.php");
    }
    exit;

  //失敗時の処理
  }else {
    setcookie('my_id', '', 0, '/test_code');
    setcookie('my_pass', '', 0, '/test_code');
    $_SESSION["save"]="";
    $_SESSION['errorMessage'] = "ユーザIDあるいはパスワードに誤りがあります。<br> (※英数字必須(5文字以上10文字以下))";
    $_SESSION['loginCount']++;
    header("Location: login.php");
  }
}



//autoログイン関数
function auto_login($token){
  $link = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
  if (!$link) {
     print(mysqli_connect_error($link));
     exit();
  }

  //SELECT文
  $result = mysqli_query($link,"SELECT id FROM token WHERE token = '$token' ");
  if (!$result) {
    print(mysqli_error($link));
    exit();
  }
  $row = mysqli_fetch_assoc($result);
  $id=$row[id];

  //SELECT文
  $result = mysqli_query($link,"SELECT pass FROM user WHERE id = '$id' ");
  if (!$result) {
    print(mysqli_error($link));
    exit();
  }
  $data = mysqli_fetch_assoc($result);
  $pass=$data[pass];
  $save='on';

  login($id,$pass,$save,$token);
}



//トークン作成の関数
function create_token() {
  $TOKEN_LENGTH = 16;//16*2=32桁
  $bytes = openssl_random_pseudo_bytes($TOKEN_LENGTH);
  return bin2hex($bytes);
}


//トークン登録の関数
function register_token($id, $token, $date) {
    //DB接続
    $dbcon = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
   if (!$dbcon) {
     print(mysqli_error($dbcon));
     exit();
   }
   $result = mysqli_query($dbcon,"SELECT id FROM token WHERE id = '$id' ");
   if (!$result) {
     print(mysqli_error($dbcon));
     exit();
   }
   $row = mysqli_fetch_assoc($result);
   $check=$row[id];

   $val1 = $id;
   $val2 = $token;
   $val3 = $date;


   if(isset($check)){
     $result = mysqli_query($dbcon,"UPDATE token SET token = '$token', cookietime='$date' WHERE id = '$id' ");
  }else{
    $query = "INSERT INTO token (id, token, cookietime) VALUES (?,?,?)";
    $stmt = mysqli_prepare($dbcon, $query);
    mysqli_stmt_bind_param($stmt, "sss", $val1, $val2, $val3);
    /* ステートメントを実行します */
    mysqli_stmt_execute($stmt);
  }
   //コネクション終了
   mysqli_close($dbcon);
}


//登校時間の登録
function time_stamp($id){
  $day=date('Y/m/d');
  $gototime=date('H:i:s');

  //DB接続
  $dbcon = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
  if (!$dbcon) {
    print(mysqli_error($dbcon));
    exit();
  }


//登校の重複が起こらないように
  mysqli_query($dbcon,"set foreign_key_checks = 0;");
  mysqli_query($dbcon,"INSERT INTO att(id,day,gotime,backtime,per1,per2,per3,per4,per5)
                        SELECT '$id','$day','$gototime','14:50:00','2','2','2','2','2'
                          FROM DUAL
                          WHERE NOT EXISTS(SELECT id,day FROM att WHERE id='$id' AND day='$day' )
                          ");

  mysqli_query($dbcon,"set foreign_key_checks = 1;");


  //コネクション終了
  mysqli_close($dbcon);
}


?>
