<?php
//外部オブジェクト
require "class/database.php";

$data = new connect;
$ev_data = $data->event();

?>
﻿<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Event Calendar Widget</title>
  <link rel="stylesheet" href="css/calendar.css">
</head>

<body>
  <div id="calendar"></div>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
  <script type="text/javascript">
  var ev_data = '<?php echo $ev_data; ?>';
  var ev_data = JSON.parse(ev_data);
  </script>
  <script type="text/javascript" src="js/calendar.js"></script>
</body>
</html>
