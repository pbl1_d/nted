﻿<?php

session_start();

//最終アクセス時間からセッション管理
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 10)) {
// 最終リクエスト時刻から10分経過した
session_unset();     //
session_destroy();   // セッション破棄
}
$_SESSION['LAST_ACTIVITY'] = time(); // 最終リクエスト時刻を更新

// echo ini_set("session.cookie_lifetime", "60");

// エラーメッセージ
$errorMessage = "";

//ログイン回数
if(!isset($_SESSION['loginCount'])){
$_SESSION['loginCount'] = 0;
}


// $_SESSION['loginCount'] = 0;

// if(isset($_POST["login"])){
//   $_SESSION['loginCount']++;
//   echo $_SESSION['loginCount'];
// }

//データベース接続
$link = mysqli_connect("localhost", "daisuke", "miyagawa", "PBLD");
if (!$link) {
   print(mysqli_connect_error($link));
   exit();
}

if(isset($_POST["id"]) || isset($_POST["pass"])){

  // エスケープする
  $id = htmlspecialchars($_POST["id"], ENT_QUOTES);
  $pass = htmlspecialchars($_POST["pass"], ENT_QUOTES);
  $save = htmlspecialchars($_POST["save"], ENT_QUOTES);

  // if($id != $_COOKIE["my_id"] || $pass != $_COOKIE["my_pass"]){
  //   $myId="";
  //   $myPass="";
  //   setcookie();
  // }

  //SELECT文
  $result = mysqli_query($link,"SELECT * FROM user WHERE id = '$id' AND pass ='$pass'");
  if (!$result) {
    print(mysqli_error($link));
    exit();

  }

  $row = mysqli_fetch_assoc($result);

  if($id == $row[id] && $pass == $row[pass]) {
    // セッションIDを新規に発行する
    session_regenerate_id(TRUE);
    $_SESSION["id"]   = $id;
    $_SESSION["pass"] = $pass;
    $_SESSION["save"] = $save;

    if($id == 'teacher'){
      header("Location: home_teacher.php");
    }else{
      header("Location: home_student.php");
    }
    exit;


  }else {
    $_SESSION["save"]="";
    $errorMessage = "ユーザIDあるいはパスワードに誤りがあります。";
    $_SESSION['loginCount']++;
    echo $_SESSION['loginCount'];
  }


}

echo'<br>';
//echo $_SESSION['loginCount'];


?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>出席管理ログイン</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/login.css" media="all" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="page-header container">
    	<h1 class="text-center">出席管理システム</h1>
    </div>

<div><?php echo $errorMessage ?></div>

<?php

if (isset($_COOKIE['my_id']) && isset($_COOKIE['my_pass'])){
    $myId = $_COOKIE['my_id'];
    $myPass = $_COOKIE['my_pass'];

}else{
    $myId = '';
    $myPass = '';
}

if(5>$_SESSION['loginCount']){

  echo '


    <div id="form" class="center-block">
     <p class="form-title lead">Login</p>
     <form action="'.htmlentities($_SERVER["PHP_SELF"]).'" method="post">
        <div class="form-group">
            <label>User ID?</label>
            <input type="text" name="id" class="form-control" value="'.htmlentities($myId).'"/>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="pass" class="form-control" value="'.htmlentities($myPass).'"/>
        </div>
        <div class="checkbox">
            <label for="save" >
                <input type="checkbox" name="save" id="save" value="on" checked="checked"/>
                パスワードを保存
            </label>
        </div>
        <button type="submit" value="login" id="login" name="login" class="btn btn-default">Login</button>

     </form>
    </div>
  ';
}
?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
