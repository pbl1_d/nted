

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ホーム</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

    <!--ヘッダ-->
    <div class="page-header">
        <h1 align="center">出席管理システム</h1>
        <p align="right">
            0X0Y0ZZ 限界 太郎
            <button type="button" class="btn btn-link">ログアウト</button>
        </p>
    </div>

    <!--メインコンテンツ-->

    <!--タブ一覧-->
    <div class="col-sm-2">
        <ul class="nav nav-pills nav-stacked">
            <li role="presentation" class="active"><a href="#menu1" data-toggle="tab">ホーム</a></li>
            <li role="presentation"><a href="#menu2" data-toggle="tab">出席状況確認(詳細)</a></li>
            <li role="presentation"><a href="#menu3" data-toggle="tab">パスワード変更</a></li>
        </ul>
    </div>

    <!--選択タブ内のコンテンツ-->
    <div class="col-sm-8 tab-content">
        <div class="tab-pane active" id="menu1">

            <!--出席状況の簡易確認-->
            <h3>本日の出席状況</h3>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>時間</th>
                        <th>出欠</th>
                        <th>理由</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1限</th>
                        <td>出席</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>2限</th>
                        <td>出席</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>3限</th>
                        <td>出席</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>4限</th>
                        <td>出席</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>5限</th>
                        <td>出席</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="menu2">

        </div>

        <div class="tab-pane" id="menu3">

        </div>
    </div>
</body>

</html>