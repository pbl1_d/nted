﻿#名前無しイメージを全削除
docker images | awk '/<none/{print $3}' | xargs docker rmi

#-----------起動しているコンテナへの接続--------------
# exitするとコンテナが終了してしまう
# コンテナを終了せずに抜ける「Ctrl + p, Ctrl + q」
docker attach CONTAINER

# exitしてもコンテナは終了しない(attachの代わり)
docker exec -it CONTAINER /bin/bash

#ビルドをキャッシュを使わずに行う
docker build --no-cache .

#立ち上げ方関連
sh ~/share/dockerstart.sh