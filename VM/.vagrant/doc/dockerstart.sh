#/bin/bash

docker run -i -t -d -v /home/vagrant/html:/var/www/html -p  80:80 --name web web/test /bin/bash
docker exec web service apache2 start
docker exec web service mysql restart
